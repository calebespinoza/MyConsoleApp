 using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Manager oManager = new Manager(1, "Marco Galvez", 6985741, 1000);
            Console.WriteLine("----- INHERANCE -----");
            oManager.printManager();
            Console.WriteLine("----- ASSOCIATION -----");
            Card oCard = new Card();
            oCard.Swipe(oManager);
            oManager.Login(oCard);
            Console.WriteLine("----- AGGREGATION -----");
            oManager.AddEmployees();
            Console.WriteLine("Marco's Employees:");
            oManager.printEmployees();
            Console.WriteLine("----- COMPOSITION -----");
            oManager.printSalary();



            //oManager.HowisTheManager(true);
            //Card objCard = new Card();
            Console.ReadKey();
        }
    }
}
