﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyConsoleApp
{
    class Manager : Employee //Inherance from Employee class
    {

        public string Role;
        public long Salary;

        //Aggregation with Employees class
        public List<Employee> Workers = null;

        //Composition with Project class
        public Project oProject;

        public Manager(int idManager, string nameManager, long dniManager, long salaryManager)
        {
            ID = idManager;
            Name = nameManager;
            DNI = dniManager;
            Role = "Project Manager";
            Salary = salaryManager;
            oProject = new Project(this.Salary, false); //Composition with Project class
        }

        public void AddEmployees()
        {
            Employee employee1 = new Employee();
            employee1.ID = 1;
            employee1.Name = "Owen Wilson";
            employee1.DNI = 5646987;
            Employee employee2 = new Employee();
            employee2.ID = 2;
            employee2.Name = "Daniel Merida";
            employee2.DNI = 7895426;
            Employee employee3 = new Employee();
            employee3.ID = 3;
            employee3.Name = "Caleb Espinoza";
            employee3.DNI = 9876542;

            Workers = new List<Employee>();
            Workers.Add(employee1);
            Workers.Add(employee2);
            Workers.Add(employee3);
        }

        public void printEmployees()
        {
            Workers.ForEach(item => Console.WriteLine(item.Name));
        }

        public void printSalary()
        {
            if (oProject.isSuccess)
                Console.WriteLine("The project was successful");
            else
                Console.WriteLine("The project was unsuccessful");
            this.Salary = oProject.estimateSalary();
            Console.WriteLine("Your salary is {0}", this.Salary);
        }

        public void printManager()
        {
            Console.WriteLine("ID: {0}", ID);
            Console.WriteLine("Nname: {0}", Name);
            Console.WriteLine("DNI: {0}", DNI);
            Console.WriteLine("Role: {0}", Role);
        }

        //Association with Card class
        public void Login(Card mySwipeCard)
        {
            string token = mySwipeCard.AccessCode();
            Console.WriteLine("Access Token: {0}", token);
        }

        public string getManagerName()
        {
            return "Caleb";
        }

        public double salary;
        public void HowisTheManager(bool Good)
        {
            if (Good)
            {
                oProject.isSuccess = true;
            }
            else
            {
                oProject.isSuccess = false;
            }
        }

    }
}
