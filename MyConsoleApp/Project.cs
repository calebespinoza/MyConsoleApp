﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyConsoleApp
{
    class Project
    {
        //Composition with Manager class
        //public Manager oManager;
        public long salary;
        public bool isSuccess;
        
        public Project(long salaryManager, bool success)
        {
            //oManager = obj;
            salary = salaryManager;
            isSuccess = success;
        }

        public long estimateSalary()
        {
            if (isSuccess)
                salary = salary + 100;
            else
                salary = salary - 100;

            return salary;
        }

        /*public bool issucess
        {
            get { return isSuccess; }
            set
            {
                isSuccess = value;
                if (value)
                {
                    oManager.salary++;
                }
                else
                {
                    oManager.salary--;
                }
            }
        }*/
    }
}
